pipeline {
    agent any
    stages {
        stage('Pull Source') {
            steps {
                git branch: "develop",
                    credentialsId: "jenkins-authen",
                    url: "git@gitlab.com:pet-project2/blog/service-background/blog-api-post-background.git"
            }
        }
    }
}

node {
    def REGISTRY_SERVER = "192.168.50.27";
    def IMAGE_NAME = "${REGISTRY_SERVER}/blog-api-post-background/post-api-background:v1.${BUILD_NUMBER}"
    def OLD_IMAGE = "${REGISTRY_SERVER}/blog-api-post-background/post-api-background:v1.${currentBuild.previousBuild.number}"
    def configuration = [vaultUrl: 'http://192.168.50.23:8200', vaultCredentialId: 'jenkins-app-role-vault', skipSslVerification: true, engineVersion: 2]

    def secrets = [
        [path: 'secret/jenkins/blog-api-post-background', engineVersion: 2, 
            secretValues: [
                [envVar: 'KAFKA_URI', vaultKey: 'Kafka.Uri'],
                [envVar: 'KAFKA_TOPIC', vaultKey: 'Kafka.Topic'],
                [envVar: 'KAFKA_GROUPID', vaultKey: 'Kafka.GroupId'],
                [envVar: 'REDIS_ABSOLUTE', vaultKey: 'Redis.AbsoluteExpiredTime'],
                [envVar: 'REDIS_PASSWORD', vaultKey: 'Redis.Password'],
                [envVar: 'REDIS_SLIDING', vaultKey: 'Redis.SlidingExpiredTime'],
                [envVar: 'REDIS_URI', vaultKey: 'Redis.Uri'],
                [envVar: 'ELASTICSEARCH_URI', vaultKey: 'Elasticsearch.Uri'],
                [envVar: 'ELASTICSEARCH_INDEX', vaultKey: 'Elasticsearch.Index']
            ]
        ]
    ]

    withVault([configuration: configuration, vaultSecrets: secrets]) {

        stage("Get And Replace Sensitive data") {
            sh "sed -i 's/{KAFKA_URI}/$KAFKA_URI/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{KAFKA_TOPIC}/$KAFKA_TOPIC/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{KAFKA_GROUPID}/$KAFKA_GROUPID/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{REDIS_URI}/$REDIS_URI/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{REDIS_PASSWORD}/$REDIS_PASSWORD/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{REDIS_ABSOLUTE}/$REDIS_ABSOLUTE/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{REDIS_SLIDING}/$REDIS_SLIDING/g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's#{ELASTICSEARCH_URI}#$ELASTICSEARCH_URI#g' ${WORKSPACE}/Appbackground/appsettings.json"
            sh "sed -i 's/{ELASTICSEARCH_INDEX}/$ELASTICSEARCH_INDEX/g' ${WORKSPACE}/Appbackground/appsettings.json"
        }

        stage('Restore Package') {
            sh "dotnet restore -v n"
        }

        stage('Publis Source') {
            sh "dotnet publish -c Release -v n -o publish --self-contained -r linux-x64"
        }

        stage("SonarQube Analysis") {
                withSonarQubeEnv("sonarqube-server") {
                sh "dotnet sonarscanner begin /k:blog-api-post-background"
                sh "dotnet build"
                sh "dotnet sonarscanner end"
            }
        }

        stage("Build Image & Push Image") {
            docker.withRegistry("https://${REGISTRY_SERVER}",'jenkins-harbor') 
            {
                def images = docker.build("${IMAGE_NAME}")
                images.push()
            }
        }

        stage("Remove Image Builded") {
            sh "docker rmi ${IMAGE_NAME}"
        }
        
        withCredentials([sshUserPrivateKey(credentialsId: "jenkins-authen", keyFileVariable: 'identity', usernameVariable: 'userName')]) {
            def server = [:]
                server.name = "k8s-master"
                server.host = "192.168.50.28"
                server.user = userName
                server.identityFile = identity
                server.allowAnyHosts = true

            stage("Stop docker image") {
                sshCommand remote: server, command: "docker stop api-post-background &>/dev/null; echo \$? > \$HOME/stop-status.log"
                sshCommand remote: server, command: "docker rm api-post-background &>/dev/null; echo \$? > \$HOME/rm-status.log"
                sshCommand remote: server, command: "docker rmi ${OLD_IMAGE} &>/dev/null; echo \$? > \$HOME/rmi-status.log"
            }

            stage("Run New Version")  {
                sshCommand remote: server, command: "docker run -d --name=api-post-background --restart=unless-stopped ${IMAGE_NAME}"
            }
        }

        stage("Remove publish folder") {
            sh "rm -rf publish"
        }
    }
}